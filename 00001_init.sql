-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE "public"."nts_current" (
  "number" text COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL,
  "route" text COLLATE "pg_catalog"."default" DEFAULT NULL,
  "ts" timestamp(0) DEFAULT NULL,
  PRIMARY KEY ("number")
)
;
-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE "public"."nts_current";
