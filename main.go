package main

import (
	"database/sql"
	"encoding/xml"
	"log"
	"os"
	"time"

	_ "github.com/lib/pq"
)

const (
	connectionString = "postgres://username:password@127.0.0.1/xmldb?sslmode=disable"
	filename         = "sample.xml"

	// The "ON CONFLICT UPDATE" part prevents you from manually checking wether the record already exists.
	// Furthermore, we will simply pass nil as ts if the xml token does not contain a time.
	// With these two "tricks", we simplify the program a great deal
	stmt = "INSERT INTO nts_current (number, route, ts) VALUES ($1, $2, $3) ON CONFLICT (number) DO UPDATE SET route=$2, ts=$3"

	timefmt = "20060102150405"
)

type ActivatedNumber struct {
	Number      string
	Route       string
	PortingTime string
}

func init() {
	log.SetFlags(log.Lshortfile | log.Ltime)
}

func main() {

	var xmlFile *os.File
	var db *sql.DB
	var err error

	// First, check wether the file can be openend for reading
	if xmlFile, err = os.Open(filename); err != nil {
		log.Fatalf("Error opening %s: %s\nAborting!", filename, err.Error())
	}
	defer xmlFile.Close()

	// Next, we create our decoder
	dec := xml.NewDecoder(xmlFile)
	dec.Strict = false

	// Ok, we have an input, now we need an output
	if db, err = sql.Open("postgres", connectionString); err != nil {
		log.Fatalf("Error occured while opening database: %s", err.Error())
	}
	defer db.Close()

	// Here is where the magic happens: We prepare a statement.
	// Think of it like a dynamic stored procedure on the database
	// server. This way, your statement does not have to be parsed
	// and compiled each time you send data to the database server.
	var upsert *sql.Stmt
	if upsert, err = db.Prepare(stmt); err != nil {
		log.Fatalf("Error while preparing upsert: %s", err.Error())
	}

	// Tell the database server to let the prepared statement go
	defer upsert.Close()
	// We define those vars outside the loop to prevent reallocations for
	// the structs we reuse anyway.
	var t xml.Token
	var an ActivatedNumber
	var ts *time.Time
	processed := 0
	start := time.Now()
	for {

		if t, err = dec.Token(); t == nil {

			log.Println("XML file processed")
			break

		} else if err != nil {

			log.Printf("Encountered error while parsing token %#v: %s", t, err.Error())
			// Depending on your use case, you might want to exit here instead
			continue

		}
		switch e := t.(type) {
		case xml.StartElement:
			if e.Name.Local == "ActivatedNumber" {

				if err = dec.DecodeElement(&an, &e); err != nil {
					log.Printf("Error decoding element: %s", err.Error())
				}

				// We only parse the time if present...
				if an.PortingTime != "" {

					if pt, err := time.Parse(timefmt, an.PortingTime); err != nil {

						// If an error occurs, we set it to nil, overwriting the value of the
						// previous iteration of the loop
						log.Printf("Error parsing timestamp '%s' of element %s", an.PortingTime, an.Number)
						ts = nil

					} else {

						// otherwise we set our ts to the parsed struct
						ts = &pt
					}
				}

				// What is that good for in the original code?
				an.Number = "90" + an.Number

				// Now, we use our prepared statement and all the values at hand to upsert the data
				// When ts is nil, a NULL value will be set in the database, which
				// is _exactly_ what would happen in case you simply omit the value.
				// If the an.Number already exists in the database, the "ON CONFLICT UPDATE"
				// clause in our prepared statement ensures that the data is simply updated,
				// as was the intention in the original code, but with more fuzz.
				//
				// NOTE: I ommited inserting into nts_history as it seems to me that it pretty
				// much holds the same values. This should be done with a view instead of an additional
				// table (see https://www.postgresql.org/docs/9.4/static/sql-createview.html for details).
				// Presumably, it would be something like
				//  CREATE VIEW nts_history AS SELECT * FROM nts_current WHERE ts < whatever
				upsert.Exec(an.Number, an.Route, ts)
				processed++
			}
		}
	}
	log.Printf("Processed %d tokens in %s", processed, time.Now().Sub(start))
}
